# Find It
**Progresso:** CONCLUÍDO<br />
**Autor:** Paulo Victor de Oliveira Leal, Wellington Santos, Kelly Hanna Malaquias, Paulo Santos<br />
**Data:** 2016<br />

### Objetivo
Implementar um aplicativo que permita pessoas que encontrem objetos perdidos, cadastrem o que e onde foi perdido, para que os devidos donos possam encontrar.

### Observação

IDE:  [Android Studio](https://developer.android.com/studio/)<br />
Linguagem: [JAVA](https://www.java.com/download/)<br />
Banco de dados: [MySQL](https://www.mysql.com/)<br />

### Execução

No Android Studio
    
### Contribuição

Esse projeto está concluído e livre para uso.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->